﻿var map, layer, markers = [];
var layerLabels;
var selectedMarker;
var doAlert = false;

document.getElementById("upload").addEventListener("change", handleFileSelect);

proj4.defs([
    [
        'WGS84',
        '+title=WGS 84 (long/lat) +proj=longlat +ellps=WGS84 +datum=WGS84 +units=degrees'
    ],
    [
        'NAD27',
        '+proj=longlat +datum=NAD27 +no_defs'
    ],
    [
        'NAD27 Alaska',
        '+proj=longlat +ellps=clrk66 +no_defs'
    ],
    [
        'NAD83',
        '+proj=longlat +ellps=GRS80 +datum=NAD83 +no_defs'
    ],
    [
        'WGS72',
        '+proj=longlat +ellps=WGS72 +no_defs'
    ],
    [
        'ETRS89',
        '+proj=longlat +ellps=GRS80 +no_defs'
    ],
    [
        'European 1950',
        '+proj=longlat +ellps=intl +no_defs'
    ],
    [
        'SAD69',
        '+proj=longlat +ellps=aust_SA +towgs84=-67.35,3.88,-38.22,0,0,0,0 +no_defs'
    ]
]);

 // nad27 alaska islands
var wkids = { 'NAD27': '4267', 'NAD27 Alaska': '37260', 'WGS72': '4322', 'ETRS89': '4258', 'European 1950': '4230', 'SAD69': '4618', 'NAD83': '4269'};

var states = { 'Alabama': 'AL', 'Alaska': 'AK', 'Arizona': 'AZ', 'Arkansas': 'AR', 'California': 'CA', 'Colorado': 'CO', 'Connecticut': 'CT', 'Delaware': 'DE', 'Florida': 'FL', 'Georgia': 'GA', 'Hawaii': 'HI', 'Idaho': 'ID', 'Illinois': 'IL', 'Indiana': 'IN', 'Iowa': 'IA', 'Kansas': 'KS', 'Kentucky': 'KY', 'Louisiana': 'LA', 'Maine': 'ME', 'Maryland': 'MD', 'Massachusetts': 'MA', 'Michigan': 'MI', 'Minnesota': 'MN', 'Mississippi': 'MS', 'Missouri': 'MO', 'Montana': 'MT', 'Nebraska': 'NE', 'Nevada': 'NV', 'New Hampshire': 'NH', 'New Jersey': 'NJ', 'New Mexico': 'NM', 'New York': 'NY', 'North Carolina': 'NC', 'North Dakota': 'ND', 'Ohio': 'OH', 'Oklahoma': 'OK', 'Oregon': 'OR', 'Pennsylvania': 'PA', 'Rhode Island': 'RI', 'South Carolina': 'SC', 'South Dakota': 'SD', 'Tennessee': 'TN', 'Texas': 'TX', 'Utah': 'UT', 'Vermont': 'VT', 'Virginia': 'VA', 'Washington': 'WA', 'West Virginia': 'WV', 'Wisconsin': 'WI', 'Wyoming': 'WY' };



function handleFileSelect(evt) {
    var files = evt.target.files; // FileList object
    var file = files[0];

    document.getElementById("instuctionsText").innerHTML = "DATA INSPECTION";
    document.getElementById("extraContent").style.display = "";

    if (file.name.indexOf(".csv") > 0) {
        document.getElementById("mapDiv").style.display = "";
        document.getElementById("dl").style.display = "";
        document.getElementById("table").style.display = "";

        var southWest = L.latLng(-90, -180),
            northEast = L.latLng(90, 180),
            bounds = L.latLngBounds(southWest, northEast);

        map = L.map('map', {
            maxBounds: bounds
        }).setView([35, -106], 2);
        layer = L.esri.basemapLayer('Imagery').addTo(map);
        layerLabels = L.esri.basemapLayer('ImageryLabels');
        map.addLayer(layerLabels);

        var searchControl = L.esri.Geocoding.geosearch().addTo(map);
        var results = L.layerGroup().addTo(map);

        searchControl.on('results', function (data) {
            results.clearLayers();
        });

        map.on('click', function (e) {
            $('.leaflet-container').css('cursor', '');

            var coords = e.latlng;
            if (document.getElementById("sampleOrder").innerHTML.length !== 0) {
                setMarkerCoord(coords.lat, coords.lng);
                setTableCoords(coords.lat, coords.lng);

                resetMarkers();
                validateCoords();
            }
        });

        // read the file metadata
        var output = '';
        output += '<span style="font-weight:bold;">' + escape(file.name) + '</span><br />\n';
        output += ' - FileType: ' + (file.type || 'n/a') + '<br />\n';
        output += ' - FileSize: ' + file.size + ' bytes<br />\n';
        output += ' - LastModified: ' + (file.lastModifiedDate ? file.lastModifiedDate.toLocaleDateString() : 'n/a') + '<br />\n';

        // read the file contents
        printTable(file);

        // post the results
        $('#list').append(output);
    }
    else {
        alert("Incorrect Format. File must be a .csv");
    }
}


function printTable(file) {
    var reader = new FileReader();
    reader.readAsText(file);
    reader.onload = function (event) {
        var csv = event.target.result;
        var data = $.csv.toArrays(csv);
        var html = '';
        for (var row in data) {
            html += '<tr>\r\n';
            for (var item in data[row]) {
                if (row == "0") {
                    html += '<td class="rows boldTable">' + data[row][item] + '</td>\r\n';
                }
                else {
                    html += '<td class="rows">' + data[row][item] + '</td>\r\n';
                }
            }
            // add new lat/long columns
            if (row == "0") {
                html += '<td class="rows boldTable"></td>\r\n<td class="rows bold"></td>\r\n';
            }
            else {
                html += '<td class="rows"></td>\r\n<td class="rows"></td>\r\n';
            }

            html += '</tr>\r\n';
        }
        $('#contents').html(html);
        createRowsAndConvert();
        setTimeout(function () { validateCoords(); doAlert = true; alert("Conversions Complete"); }, 8000);
    };
    reader.onerror = function () { alert('Unable to read ' + file.fileName); };
}


function createRowsAndConvert() {
    var latrow, lonRow, datumRow;

    var table = document.getElementById("contents");
    for (var i = 0, row; row = table.rows[i]; i++) {
        if (row.cells[0].innerText !== "SAMPLE_ORDER") {
            var lat = row.cells[latrow].innerText, lon = row.cells[lonRow].innerText;
            var system = row.cells[datumRow].innerText;

            convertCoords(lat, lon, system, row.cells[0].innerText);
        }
        else {
            row.cells[row.cells.length - 2].innerText = "LAT_WGS84";
            row.cells[row.cells.length - 1].innerText = "LON_WGS84";

            for (var m = 0; m < row.cells.length; m++) {
                if (row.cells[m].innerText == "LAT_DEC") {
                    latrow = m;
                }
                else if (row.cells[m].innerText == "LON_DEC") {
                    lonRow = m;
                }
                else if (row.cells[m].innerText == "DATUM") {
                    datumRow = m;
                }
            }
        }
    }
}


function setTableRow(sampleOrder, valid) {
    var cls = "selected";

    if (valid == "wrong") {
        cls = "incorrect";
    }
    var table = document.getElementById("contents");
    for (var i = 0, row; row = table.rows[i]; i++) {
        if (row.cells[0].innerText !== "SAMPLE_ORDER") {
            if (row.cells[0].innerText === sampleOrder) {
                if (valid == "correct") {
                    row.className = "";
                }
                else {
                    if (row.className.indexOf("incorrect") < 0 || cls != "incorrect") {
                        row.className += " " + cls;
                    }
                }
            }
        }
    }
}


function resetTable() {
    var table = document.getElementById("contents");
    for (var i = 0, row; row = table.rows[i]; i++) {
        if (row.className.indexOf("incorrect") < 0) {
            row.className = "";
        }
        else {
            row.className = "incorrect";
        }
    }
}


function resetSelection() {
    //document.getElementById("sampleOrder").innerHTML = "";
    //document.getElementById("lat").innerHTML = "";
    //document.getElementById("lon").innerHTML = "";

    document.getElementById("setCoord").style.display = "none";
    document.getElementById("editCoord").style.display = "none";
    document.getElementById("resetCoords").style.display = "none";

    resetMarkers();
}


function resetMarkers() {
    map.eachLayer(function (layer) {
        var features = [];
        map.eachLayer(function (layer) {
            if (layer instanceof L.Marker) {
                if (map.getBounds().contains(layer.getLatLng())) {
                    layer._icon.src = "https://unpkg.com/leaflet@1.3.1/dist/images/marker-icon.png"
                    
                    layer.update();
                }
            }
        });
    });
}


function tableToArray() {
    var myTableArray = [];

    $("table#contents tr").each(function () {
        var arrayOfThisRow = [];
        var tableData = $(this).find('td');
        if (tableData.length > 0) {
            tableData.each(function () { arrayOfThisRow.push($(this).text()); });
            myTableArray.push(arrayOfThisRow);
        }
    });

    let csvContent = "data:text/csv;charset=utf-8,";
    myTableArray.forEach(function (rowArray) {
        let row = rowArray.join(",");
        csvContent += row + "\r\n";
    }); 

    var csvName = document.getElementById("upload").value.replace("C:\\fakepath\\", "").replace(".csv", "_updated.csv");
    
    var encodedUri = encodeURI(csvContent);
    var link = document.createElement("a");
    link.setAttribute("href", encodedUri);
    link.setAttribute("download", csvName);
    link.innerHTML = "Click Here to download";
    document.body.appendChild(link); // Required for FF

    link.click();
}


function convertAGS(inWkid, lat, lon, id) {
    if (inWkid == "WGS84") {
        //console.log(lon, lat, id);

        createMarkers(lon, lat, id);
        set(lat, lon, true, id);
    }
    else {
        require(["esri/geometry/support/GeographicTransformation", "esri/geometry/projection", "esri/geometry/SpatialReference", "esri/geometry/Point", "esri/Graphic"], function (GeographicTransformation, projection, SpatialReference, Point, Graphic) {

            if (!projection.isSupported()) {
                console.error("Conversion/Projection is not supported on this browser. Please Use Chrome or Firefox.");
                return;
            }

            var outSpatialReference = new SpatialReference({ wkid: 4326 });
            var inSR = new SpatialReference({ wkid: wkids[inWkid] });

            var point = {
                type: "point",
                longitude: lon,
                latitude: lat,
                spatialReference: inSR
            };

            var pointGraphic = new Graphic({
                geometry: point
            });

            projection.load().then(function () {
                var coords = projection.project(pointGraphic.geometry, outSpatialReference);
                //console.log(coords.x, coords.y, id);

                createMarkers(coords.x, coords.y, id);
                set(coords.y, coords.x, id);
                resetMarkers();
            });
        });
    }
}

function createMarkers(x, y, id) {

    var marker = L.marker([y, x]).addTo(map).on('click', function (e) {
        resetMarkers();
        resetTable();
        var currentCoords = e.latlng;
        $('.leaflet-container').css('cursor', 'pointer');

        document.getElementById("sampleOrder").innerHTML = e.target._icon.id;
        document.getElementById("lat").innerHTML = currentCoords.lat;
        document.getElementById("lon").innerHTML = currentCoords.lng;
        setTableRow(e.target._icon.id);
        document.getElementById("tableData").style.display = "";
        document.getElementById("editCoord").style.display = "";
        document.getElementById("resetCoords").style.display = "";

        e.target._icon.src = "https://raw.githubusercontent.com/pointhi/leaflet-color-markers/master/img/marker-icon-green.png";
    });
    marker._icon.id = id;

    markers.push(marker);

    //if (coords) {
    //    row.cells[row.cells.length - 2].innerText = coords[1];
    //    row.cells[row.cells.length - 1].innerText = coords[0];
    //}
}


function convertCoords(lat, lon, system, id) {
    //if (system !== "WGS84") {
        //var point = [Number(lon), Number(lat)];
        try {
            //var coords = proj4(proj4(system), proj4("WGS84"), point);
            convertAGS(system, lat, lon, id);
        }
        catch (error) {
            if (system.length === 0) {
                alert("Reference point encountered.");
            }
            else {
                alert("DATUM/Coordinate System: " + system + " caused Error.");
            }
        }
    //}
    //else {
    //    coords = [lon, lat];
    //}
    
    //return coords;
}


function setMarkerCoord(lat, lon) {
    var id = document.getElementById("sampleOrder").innerHTML;

    map.eachLayer(function (layer) {
        var features = [];
        map.eachLayer(function (layer) {
            if (layer instanceof L.Marker) {
                if (map.getBounds().contains(layer.getLatLng()) && layer._icon.id === id) {
                    layer._latlng.lat = lat;
                    layer._latlng.lng = lon;

                    document.getElementById("lat").innerHTML = lat;
                    document.getElementById("lon").innerHTML = lon;

                    layer.update();
                }
            }
        });
    });

    resetTable();
    resetSelection();
    alert("Sample Order: " + id + " moved to Latitude: " + lat + ", " + "Longitude: " + lon + ".");
    document.getElementById("tableData").style.display = "none";
}


function setTableCoords(lat, lon, id) {
    var table = document.getElementById("contents");
    for (var i = 0, row; row = table.rows[i]; i++) {
        if (row.cells[0].innerText !== "SAMPLE_ORDER") {
            if (row.cells[0].innerText === document.getElementById("sampleOrder").innerHTML ||
                row.cells[0].innerText === id) {
                row.cells[row.cells.length - 2].innerText = lat;
                row.cells[row.cells.length - 1].innerText = lon;
            }
        }
    }
}


function edit() {
    document.getElementById("lat").style.display = "none";
    document.getElementById("lon").style.display = "none";

    document.getElementById("inputLat").style.display = "";
    document.getElementById("inputLat").value = document.getElementById("lat").innerHTML;
    document.getElementById("inputLon").style.display = "";
    document.getElementById("inputLon").value = document.getElementById("lon").innerHTML;

    document.getElementById("setCoord").style.display = "";
    document.getElementById("editCoord").style.display = "none";
}


function set(lat, lon, id) {
    if (!lat) {
        var lat = document.getElementById("inputLat").value;
        var lon = document.getElementById("inputLon").value;
    }

    if (doAlert) {
        setMarkerCoord(lat, lon);
    }
    setTableCoords(lat, lon, id);
    
    document.getElementById("lat").style.display = "";
    document.getElementById("lon").style.display = "";
    document.getElementById("inputLat").style.display = "none";
    document.getElementById("inputLon").style.display = "none";

    document.getElementById("setCoord").style.display = "none";
    document.getElementById("editCoord").style.display = "";
}


function setBasemap(basemap) {
    if (layer) {
        map.removeLayer(layer);
    }

    layer = L.esri.basemapLayer(basemap);

    map.addLayer(layer);

    if (layerLabels) {
        map.removeLayer(layerLabels);
    }

    if (basemap === 'ShadedRelief'
        || basemap === 'Oceans'
        || basemap === 'Gray'
        || basemap === 'DarkGray'
        || basemap === 'Terrain'
    ) {
        layerLabels = L.esri.basemapLayer(basemap + 'Labels');
        map.addLayer(layerLabels);
    } else if (basemap.includes('Imagery')) {
        layerLabels = L.esri.basemapLayer('ImageryLabels');
        map.addLayer(layerLabels);
    }
}


function changeBasemap(basemaps) {
    var basemap = basemaps.value;
    setBasemap(basemap);
}


function validateCoords() {
    var latrow, lonRow, datumRow, countryRow, stateRow;
    var table = document.getElementById("contents");
    for (var i = 0, row; row = table.rows[i]; i++) {
        if (row.cells[0].innerText !== "SAMPLE_ORDER") {
            var lat = row.cells[latrow].innerText;
            var lng = row.cells[lonRow].innerText;
            var state = row.cells[stateRow].innerText;
            var country = row.cells[countryRow].innerText;
            var id = row.cells[0].innerText;

            if (lat == 0 && lng == 0) {
                setTableRow(id, "wrong");
            }
            else {
                if (row.cells[countryRow].innerText == "United States") {
                    lookUpCoord(lat, lng, state, id, "https://sampleserver6.arcgisonline.com/arcgis/rest/services/USA/MapServer/3/query/", "state_name");
                }
                else {
                    lookUpCoord(lat, lng, country, id, "https://services.arcgis.com/P3ePLMYs2RVChkJx/ArcGIS/rest/services/World_Countries/FeatureServer/0/query/", "COUNTRY");
                }
            }
        }
        else {
            for (var m = 0; m < row.cells.length; m++) {
                if (row.cells[m].innerText == "LAT_WGS84") {
                    latrow = m;
                }
                else if (row.cells[m].innerText == "LON_WGS84") {
                    lonRow = m;
                }
                else if (row.cells[m].innerText == "DATUM") {
                    datumRow = m;
                }
                else if (row.cells[m].innerText == "COUNTRY") {
                    countryRow = m;
                }
                else if (row.cells[m].innerText == "STATE_PROV") {
                    stateRow = m;
                }
            }
        }
    }
}


function lookUpCoord(lat, lng, location, id, url, field) {
    // ajax query to ags mapserver layer that has counties attributed with county name and state
    var result;
    $.ajax({
        url: url, // founds this from google search: "arcgis map server counties"
        data: {
            geometry: [lng, lat].join(","), // "lng,lat" (esri is x,y)
            geometryType: "esriGeometryPoint",
            inSR: 4326,                // wgs84
            returnGeometry: false,               // set false for compact response of just state-county info - but you could set true and directly add geojson county polygon to a Leaflet map
            outFields: field,   // there are more attributes available that could be useful - set "*" to return all
            f: "geojson"
        },
        method: "GET",
        dataType: "json",
        async: true,
        cache: true,
        error: function (xhr) {
            // request error
            //console.warn("error querying ags: (" + xhr.status + ") " + xhr.statusText);
        },
        success: function (geojson) {
            // got response
            //console.warn(geojson); // in case you want to see response in dev console
            if (geojson.features !== undefined && geojson.features.length === 1 && geojson.features[0].properties !== undefined) { // validate response has what we need
                result = geojson.features[0].properties; // response has single feature with properties - assign the properties
            }
        },
        complete: function () {
            // executes whether error or success

            if (result) {
                highlightIfWrong(location, result[field], field, id);
            }
            else {
                setTableRow(id, "wrong");
            }

            
        }
    });
}


function highlightIfWrong(location, result, field, id) {
    //console.log(result, field);

    if (field == "state_name") {
        if (location != states[result]) {
            setTableRow(id, "wrong");
        }
        else {
            setTableRow(id, "correct");
        }
    }
    else {
        if (location != result) {
            setTableRow(id, "wrong");
        }
        else {
            setTableRow(id, "correct");
        }
    }
}


function resetCoords() {
    var sampleId = document.getElementById("sampleOrder").innerHTML;
    var table = document.getElementById("contents");
    var latRow, lonRow, datumRow;

    for (var i = 0, row; row = table.rows[i]; i++) {
        if (row.cells[0].innerText == sampleId) {
            var lat = row.cells[latrow].innerText, lon = row.cells[lonRow].innerText;
            var system = row.cells[datumRow].innerText;

            convertAGS(system, lat, lon, row.cells[0].innerText);
        }
        else {
            for (var m = 0; m < row.cells.length; m++) {
                if (row.cells[m].innerText == "LAT_DEC") {
                    latrow = m;
                }
                else if (row.cells[m].innerText == "LON_DEC") {
                    lonRow = m;
                }
                else if (row.cells[m].innerText == "DATUM") {
                    datumRow = m;
                }
            }
        }
    }
}